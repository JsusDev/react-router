import React from 'react'
import { Link } from 'react-router-dom'
import Style from './navigation/style.css'

const LinkStyle = props => (
	<Link className='link' to={props.to}>
		<div>{props.title}</div>
	</Link>
)


class Navigation extends React.Component {

	render() {
		return (
			<div className='menu'>
				<Link className='link' to='/'>Главная Страница</Link>
				<Link className='link' to='/papers'>Статьи</Link>
				<Link className='link' to='/examples'>Примеры</Link>
				<Link className='link' to='/contacts'>Контакты</Link>
				<Link className='link' to='/about'>Обо мне</Link>
			</div>
		)
	}

}


export default Navigation