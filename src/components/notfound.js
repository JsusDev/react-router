import React from 'react'
import Ghost from './notfound/ghost_PNG35.png'
import Style from './notfound/style.css'
import { Link } from 'react-router-dom'



class NotFound extends React.Component {

	render() {
		return (
			<div className='page'>
				<img className='ghost' src={Ghost} />
				<div>
					<div className='notfound'>Нет такой страницы</div>
					<Link className='link' to='/'>Главная Страница</Link>
				</div>
			</div>
		)
	}

}


export default NotFound