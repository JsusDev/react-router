import React from 'react'
import { BrowserRouter, Route, Switch, Link } from 'react-router-dom'
import Style from './App.css'

import Navigation from './components/navigation'

import NotFound  from './components/notfound'
import Home 	 from './components/home'
import Contacts	 from './components/contacts'
import Papers	 from './components/papers'
import Examples  from './components/examples'
import About     from './components/about'

import LeftConetent from './components/left-content'


class App extends React.Component {

	render() {
		return (
			<BrowserRouter>
				<div>
					<Navigation />
					
					<div className='wrapper'>
						<div className='content'>
							<Switch>
								<Route path='/' exact component={Home} />
								<Route path='/contacts' component={Contacts} />
								<Route path='/papers' component={Papers} />
								<Route path='/examples' component={Examples} />
								<Route path='/about' component={About} />
								<Route component={NotFound} />
							</Switch>
						</div>
						<div className='content other-content'>
							<LeftConetent />
						</div>
					</div>
				</div>
			</BrowserRouter>
		)
	}

}

export default App 